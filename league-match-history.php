<?php include_once "./app/SQLiteConnection.php"; ?>

<?php include_once "./header.php" ?>

<div class="container-fluid my-2">
	<h2>League Match History</h2>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<td>Date</td>
					<td>Player</td>
					<td>Wins</td>
					<td>Player</td>
					<td>Wins</td>
				</tr>
			</thead>
			<tbody>
				<?php
					$connection = new SQLiteConnection();
					$history = $connection->getLeagueMatchHistory(Config::ACTIVE_LEAGUE);
					date_default_timezone_set('America/Los_Angeles');
					
					foreach ($history as $match) {
						echo "<tr><td>" . date('m/d/Y h:i A', $match['timestamp']) . "</td><td>";
						echo $match['playerOne'] . "</td><td>" . $match['playerOneWins'] . "</td><td>";
						echo $match['playerTwo'] . "</td><td>" . $match['playerTwoWins']."</td></tr>";
						
					}
				?>
			</tbody>
		</table>
	</div>	
</div>
<?php include_once "./footer.php" ?>