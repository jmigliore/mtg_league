<?php include_once "app/SQLiteConnection.php"; ?>

<?php include_once "./header.php" ?>
<div class="container-fluid my-2">
	<?php
		$connection = new SQLiteConnection();
		$info = $connection->getLeagueInfo(Config::ACTIVE_LEAGUE);
	?>
	<h2>League Standings</h2>
	<h6><?php echo $info[0]['name'] . " (" . date('M j, Y',strtotime($info[0][startDate])) . " - " . date('M j, Y',strtotime($info[0][endDate])) . ")";?></h6>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<td>Rank</td>
					<td>Player</td>
					<td>Rating</td>
					<td>Wins</td>
					<td>Losses</td>
				</tr>
			</thead>
			<tbody>
				<?php
					$standings = $connection->getLeagueStandings(Config::ACTIVE_LEAGUE);
					$rank = 1;
					
					foreach ($standings as $player) {
						echo "<tr><td>". $rank ."</td><td>" . $player['name'] . "</td><td>" . $player['rating'] . "</td><td>" . $player['wins'] . "</td><td>" . $player['losses'] ."</td></tr>";
						$rank++;
					}
				?>
			</tbody>
		</table>
	</div>

</div>
<?php include_once "./footer.php" ?>