<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/mtg_league/app/Rating.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/mtg_league/app/SQLiteConnection.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/mtg_league/app/Validator.php";

//echo json_encode($_POST);
if (sizeof($_POST) > 0){
    $response['service'] = empty($_GET['service'])?(empty($_POST['service'])?FALSE:$_POST['service']):$_GET['service'];

    switch ($response['service']) {

        case 'SUBMIT_MATCH_RESULT':
			$response = submitMatchResult($response);
			break;
		case 'NEW_PLAYER':
			addNewPlayer();
			break;
		case 'SIGNUP':
			$response = signUp($response);
			break;
        default:
            $response['error'] = 'Invalid AJAX service requested: "'.$response['service'].'"';
    }
    echo json_encode($response);

}else{
    exit;
}

function addNewPlayer(){
	$data = cleanInput();

	$connection = new SQLiteConnection();
	$connection->addNewPlayer($data[firstName],$data[lastName],$data[email]);
}

function signUp($response){
	$data = cleanInput();

	$connection = new SQLiteConnection();
	$valid = Validator::validateSignUp($data[playerID], $data[leagueID]);
	if($valid == 1){
		$connection->signUp($data[playerID],$data[leagueID],$data[broughtPacks]);	
		$response['playerCount'] = count($connection->getLeaguePlayers($data[leagueID]));	
	}else{
		$response['error'] = $valid;
	}
	return $response;
}

function submitMatchResult($response){
	$data = cleanInput();
	
	if(!$data[playerOneWins]){
		$data[playerOneWins] = 0;
	}
	if(!$data[playerTwoWins]){
		$data[playerTwoWins] = 0;
	}
	$valid = Validator::validateLeagueMatch($data[playerOneID],$data[playerOneWins],$data[playerTwoID],$data[playerTwoWins],$data[leagueID]);
	if($valid == 1){
		$response = $response + Rating::reportLeagueMatch($data[playerOneID],$data[playerOneWins],$data[playerTwoID],$data[playerTwoWins],$data[leagueID]);
	}else{
		$response['error'] = $valid;
	}
	return $response;
	
}

function cleanInput(){
	$whitelist = array(
		'firstName',
		'lastName',
		'email',
		'playerID',
		'leagueID',
		'broughtPacks',
		'playerOneID',
		'playerOneWins',
		'playerTwoID',
		'playerTwoWins',
		);
		
    foreach($whitelist as $key) {
	    if (isset($_POST[$key])) {
	        $data[$key] = htmlspecialchars($_POST[$key]);
	    }
    }
	return $data;
}
?>