<?php include_once "./header.php" ?>

<div class="container my-2">
	<h2>New Player</h2>
	<form role="form" id="new-player-form">
		<div class="form-row">
			<div class="col-6">
				<label class="p-2">First Name</label>
				<input type="text" class="form-control" id="firstName" placeholder="First name" required>
			</div>
			<div class="col-6">
				<label class="p-2">Last Name</label>
				<input type="text" class="form-control" id="lastName" placeholder="Last name" required>
			</div>
		</div>
		<div class="form-row">
			<div class="col-6">
				<label class="p-2">E-mail</label>
				<input type="email" class="form-control" id="email" placeholder="Email" required>
			</div>
		</div>
		<div id="feedback" role="alert"></div>
		<button type="submit" class="btn btn-primary mt-2" id="submit">Submit</button>
	</form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
    		//Validation
    		function isEmail(email) {
  			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  return regex.test(email);
		}
        //Ajax
        var submitted = false;
        $('#new-player-form').on('submit', function(event) {
            
            if(!isEmail($('#email').val())){
            		$('#feedback').html("Please add a valid e-mail.");
            		$('#feedback').addClass("mt-2 alert alert-danger");
            		return false;               	
            }        	
            var data = {};
                 
            data.service = 'NEW_PLAYER';
            data.firstName =       $('input[id=firstName]').val();
            data.lastName =       $('input[id=lastName]').val();
            data.email =       $('input[id=email]').val(); 

            if(!submitted){
                submitted = true;

                $.ajax({
                    type:		'POST',
                    data:		data,
                    url:		'./ajax/ajax-handler.php',
                    dataType:	'json',
                    error:		function(jqXHR,textStatus,errorThrown){},
                    success:	function(response,textStatus,jqXHR){
                        if (response.error) {
                            alert(response.error);
                            submitted = false;
                        } else {
			                var html ="<div class='col-sm-12 text-center'><h1>Welcome to the MTG League!</h1></div>";
               				$('#new-player-form').html(html);
                        }
                    },
                    complete:	function(jqXHR,textStatus){}
                });

            }
            return false;

        });
    });
</script>
<?php include_once "./footer.php" ?>