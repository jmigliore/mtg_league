<?php include_once "app/SQLiteConnection.php"; ?>

<?php include_once "./header.php" ?>
<div class="container-fluid my-2">
	<?php
		$connection = new SQLiteConnection();
		$info = $connection->getLeagueInfo(Config::ACTIVE_LEAGUE);
	?>
	<h2>League Registration</h2>
	<h6><?php echo $info[0]['name'] . " (" . date('M j, Y',strtotime($info[0][startDate])) . " - " . date('M j, Y',strtotime($info[0][endDate])) . ")";?></h6>	

	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<td>Player</td>
					<td>Paid</td>
					<td>Brought Packs</td>
					<td>Comment</td>
				</tr>
			</thead>
			<tbody>
				<?php
					$connection = new SQLiteConnection();
					$registerPlayers = $connection->getLeagueRegisteredPlayers(Config::ACTIVE_LEAGUE);
					
					foreach ($registerPlayers as $player) {
						echo "<tr><td>" . $player['name'] . "</td><td>" . ($player['paid'] == 0 ? 'No' : 'Yes') . "</td><td>" . ($player['broughtPacks'] == 0 ? 'No' : 'Yes') . "</td><td>" . $player['comment'] ."</td></tr>";
						
					}
				?>
			</tbody>
		</table>
	</div>

</div>
<?php include_once "./footer.php" ?>