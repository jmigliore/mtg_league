<?php include_once "./app/SQLiteConnection.php"; ?>

<?php include_once "./header.php" ?>

<div class="container my-2">
	<h2>Sign Up for League</h2>
	<h6><a href="league-registration.php">view league registration</a></h6>

	<form role="form" id="signup-form">
		<div class="form-row">
			<div class="col-6">
				<label class="p-2">Player</label>
				<select class="form-control" id="playerID" required>
					<option></option>
					<?php
						$connection = new SQLiteConnection();
						$players = $connection->getPlayers();
					
						foreach ($players as $player) {
							echo "<option value=" . $player['idplayer'] .">" . $player['name'] . "</option>";
						}
					?>
				</select>
				<small class="form-text"><a href="./new-player.php">new player?</a></small>
			</div>
			<div class="col-6">
				<label class="p-2">League</label>
				<select class="form-control" id="leagueID" required>
					<?php
						$leagues = $connection->getLeagues();
						foreach ($leagues as $league) {
							echo "<option value=" . $league['idleague'] .">" . $league['name'] . "</option>";
						}
					?>
				</select>
			</div>
		</div>
		<div class="form-group pt-2">
			<div class="form-check">			
				<input class="form-check-input" type="checkbox" id="broughtPacks">
				<label class="form-check-label">I have my own packs</label>
			</div>
		</div>	

		<button type="submit" class="btn btn-primary mt-2" id="submit">Submit</button>
	</form>
</div>	
<script type="text/javascript">
    $(document).ready(function() {
        //Ajax
        var submitted = false;
        $('#signup-form').on('submit', function(event) {
        	
            var data = {};
                 
            data.service = 'SIGNUP';
            data.playerID =       $('select[id=playerID]').val();
            data.leagueID =       $('select[id=leagueID]').val(); 
            data.broughtPacks =   $('#broughtPacks').is(':checked');

			console.log(data);

            if(!submitted){
                submitted = true;

                $.ajax({
                    type:		'POST',
                    data:		data,
                    url:		'./ajax/ajax-handler.php',
                    dataType:	'json',
                    error:		function(jqXHR,textStatus,errorThrown){},
                    success:	function(response,textStatus,jqXHR){
                    	console.log(response);
                        if (response.error) {
                            alert(response.error);
                            submitted = false;
                        } else {
						    var html ="<div class='col-sm-12 text-center'><h1>Welcome to the ";
			                html += $('select[id=leagueID] option:selected').text();
			                html += " league!</h1><p>There are now ";
			                html += response.playerCount;
			                html += " players signed up.</p></div>" ;
			 
			                $('#signup-form').html(html);
                        }
                    },
                    complete:	function(jqXHR,textStatus){}
                });

            }
            return false;

        });
    });
</script>
<?php include_once "./footer.php" ?>