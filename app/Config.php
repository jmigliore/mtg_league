<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/mtg_league/app/SQLiteConnection.php";

class Config {
	const KVALUE = '16';
	const STARTING_RATING = '1600';
	const ACTIVE_LEAGUE = '4';
	
	/*const TABLE_SETTINGS = 
	'CREATE TABLE settings(
		kValue INTEGER,
		startingRating INTEGER					
	)';*/
	public static function getDatabasePath(){
		return $_SERVER['DOCUMENT_ROOT'] . '/mtg_league/db/mtg_league.db';
	}
	const TABLE_LEAGUE =
	'CREATE TABLE league(
		idleague INTEGER PRIMARY KEY NOT NULL,
		name TEXT NOT NULL,
		startDate DATETIME NOT NULL,
		endDate DATETIME NOT NULL
	)';
	const TABLE_PERIOD = 
	'CREATE TABLE period(
		idperiod INTEGER PRIMARY KEY NOT NULL,
		startDate DATETIME NOT NULL,
		endDate DATETIME NOT NULL,
		league_idleague INTEGER NOT NULL,
		FOREIGN KEY(league_idleague) REFERENCES league(idleague)	
	)';	
	const TABLE_PLAYER = 
	'CREATE TABLE player(
		idplayer INTEGER PRIMARY KEY NOT NULL,
		firstName TEXT NOT NULL,
		lastName TEXT NOT NULL,
		rating INTEGER,
		email TEXT				
	)';
	const TABLE_LEAGUE_HAS_PLAYER = 
	'CREATE TABLE league_has_player(
		paid BOOLEAN,
		broughtPacks BOOLEAN,
		comment TEXT,
		rating INTEGER,
		league_idleague INTEGER NOT NULL,
		player_idplayer INTEGER NOT NULL,
		FOREIGN KEY(league_idleague) REFERENCES league(idleague),
		FOREIGN KEY(player_idplayer) REFERENCES player(idplayer)						
	)';	
	const TABLE_MATCH = 
	'CREATE TABLE match(
   	idmatch INTEGER PRIMARY KEY NOT NULL,
   	playerOneWins INTEGER,
   	playerTwoWins INTEGER,
   	draw BOOLEAN,
   	timestamp DATETIME,
   	playerOne_idplayer INTEGER NOT NULL,
   	playerTwo_idplayer INTEGER NOT NULL,
   	league_idleague INTEGER NOT NULL,
   	FOREIGN KEY(playerOne_idplayer) REFERENCES player(idplayer),
   	FOREIGN KEY(playerTwo_idplayer) REFERENCES player(idplayer),
   	FOREIGN KEY(league_idleague) REFERENCES league(idleague)		
	)';		

	public function createDatabase(){
		$connection = new SQLiteConnection();

		$tables = array(
			Config::TABLE_SETTINGS, 
			Config::TABLE_LEAGUE, 
			Config::TABLE_PERIOD, 
			Config::TABLE_PLAYER,
			Config::TABLE_LEAGUE_HAS_PLAYER,
			Config::TABLE_MATCH
			);
			
		foreach($tables as $table){
			echo $table . "<br>";
			$connection->run($table);
		}
				
	}	
}
?>