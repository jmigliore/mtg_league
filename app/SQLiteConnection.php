<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/mtg_league/app/Config.php";

Class SQLiteConnection {
	private $pdo;
	
	public function __construct(){
		$this->connect();
	}	
	
	public function connect() {
		if ($this->pdo == null) {
            $this->pdo = new PDO("sqlite:" . Config::getDatabasePath());
        }
		$this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); //remove extra keys in results
	}
	
	private function fetchAll($statement){
		return $this->pdo->query($statement)->fetchAll();
	}
	private function run($statement){
		return $this->pdo->exec($statement);
	}
	
	public function addNewPlayer($firstName, $lastName, $email){
		if(!$email){ $email = "NULL";}else{ $email = '"' . $email .'"';}
		
		$statement = 'insert into player 
						(firstName, lastName, email, rating)
						values
						("' . $firstName . '","' . $lastName . '",' . $email .',' . Config::STARTING_RATING .');';
		return $this->run($statement);
	}
	
	public function signUp($playerID, $leagueID, $broughtPacks){
		if($broughtPacks == 'false'){$broughtPacks = 0;}else{$broughtPacks = 1;};
		
		$statement = 'insert into league_has_player
						(player_idplayer, league_idleague, broughtPacks, rating)
						values
						(' . $playerID . ',' . $leagueID . ',' . $broughtPacks .',' . Config::STARTING_RATING .');';
		return $this->run($statement);
	}	
	public function getPlayers(){
		$statement = 'SELECT 
				player.idplayer,
				player.firstName|| " " ||player.lastName as name
				FROM player
				ORDER BY LOWER(player.firstname);';
		return $this->fetchAll($statement);
	}
	
	public function getLeagues(){
		$statement = 'SELECT
					idleague,
					name
					FROM league
					ORDER BY idleague DESC;';
		return $this->fetchAll($statement);		
	}
	
	public function getLeagueInfo($leagueID){
		$statement = 'SELECT
					name,
					startDate,
					endDate
					FROM league
					WHERE idleague = "'. $leagueID . '";';
		return $this->fetchAll($statement);			
	}
		
	public function getLeagueRating($playerID, $leagueID){
		$statement = 'SELECT 
					rating 
					FROM league_has_player 
					WHERE league_idleague = "' . $leagueID . '" 
					AND player_idplayer = "' . $playerID . '"';				
		$row = $this->fetchAll($statement);
		
		return $row[0]['rating'];

	}

	public function getLeagueStandings($leagueID){
		$statement = 'select 
					league_has_player.rating, 
					player.firstName|| " " ||player.lastName as name 
					from league_has_player
					inner join player on player.idplayer = league_has_player.player_idplayer 
					where league_idleague = ' . $leagueID . '
					order by league_has_player.rating desc;';
		 $standings = $this->fetchAll($statement);
		 
		 foreach($standings as &$result){
		 	$result['wins'] = 0;
			$result['losses'] = 0;
		 }

		 $statement = 'select
	 				playerOneWins,
	 				playerTwoWins,
	 				p1.firstName|| " " ||p1.lastName as playerOne,
	 				p2.firstName|| " " ||p2.lastName as playerTwo
	 				from match
	 				inner join player p1 on p1.idplayer = match.playerOne_idplayer
	 				inner join player p2 on p2.idplayer = match.playerTwo_idplayer
	 				where league_idleague = ' . $leagueID . '
	 				;';
		$matches = 	$this->fetchAll($statement);
		
		foreach($matches as $match){
			if($match[playerOneWins] == 2){
				$done = 0;
				foreach($standings as &$result){
					if($result['name'] == $match['playerOne']){
						$result['wins'] = $result['wins'] + 1;						
						$done++;
					}
					if($result['name'] == $match['playerTwo']){
						$result['losses'] = $result['losses'] + 1; 
						$done++;
					}
					if(done == 2){
						break;
					}
				}
			}else if($match[playerTwoWins] == 2){
				$done = 0;
				foreach($standings as &$result){
					if($result['name'] == $match['playerTwo']){
						$result['wins'] = $result['wins'] + 1;
						$done++;
					}
					if($result['name'] == $match['playerOne']){
						$result['losses'] = $result['losses'] + 1; 
						$done++;
					}
					if(done == 2){
						break;
					}
				}				
			}
		}	
		
		return $standings;
	}
	
	public function getLeaguePlayers($leagueID){
		$statement = 'SELECT 
				player.idplayer,
				player.firstName|| " " ||player.lastName as name
				from league_has_player
				inner join player on player.idplayer = league_has_player.player_idplayer 
				where league_idleague = ' . $leagueID . '
				order by LOWER(player.firstname);';
		return $this->fetchAll($statement);	
	}

	public function getLeagueRegisteredPlayers($leagueID){
		$statement = 'SELECT 
				player.idplayer,
				player.firstName|| " " ||player.lastName as name,
				league_has_player.paid,
				league_has_player.broughtPacks,
				league_has_player.comment
				from league_has_player
				inner join player on player.idplayer = league_has_player.player_idplayer 
				where league_idleague = ' . $leagueID . '
				order by LOWER(player.firstname);';
		return $this->fetchAll($statement);	
	}

	public function getLeagueMatchHistory($leagueID){
		 $statement = 'select
		 	timestamp,
			playerOneWins,
			playerTwoWins,
			p1.firstName|| " " ||p1.lastName as playerOne,
			p2.firstName|| " " ||p2.lastName as playerTwo
			from match
			inner join player p1 on p1.idplayer = match.playerOne_idplayer
			inner join player p2 on p2.idplayer = match.playerTwo_idplayer
			where league_idleague = ' . $leagueID . '
			order by timestamp desc;';
		return $this->fetchAll($statement);
	}
	

	public function setLeagueRating($playerID, $rating, $leagueID){
		$statement = 'UPDATE league_has_player
					SET rating = ' . $rating . '
					WHERE league_idleague = '  . $leagueID . ' 
					AND player_idplayer = ' . $playerID . ';';
		return $this->run($statement);
	}
	
	public function submitLeagueMatch($playerOneID, $playOneWins, $playerTwoID, $playTwoWins, $leagueID){
		$timestamp = time();
		$statement = 'insert into match
				(playerOneWins,
			   	playerTwoWins,
			   	timestamp,
			   	playerOne_idplayer,
			   	playerTwo_idplayer,
			   	league_idleague)
				values
				(' . $playOneWins . ',' . $playTwoWins . ',' . $timestamp . ',' . $playerOneID . ',' . $playerTwoID . ',' . $leagueID .');';
		return $this->run($statement);		
		
	}	
}

?>