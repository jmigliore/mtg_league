<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/mtg_league/app/Config.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/mtg_league/app/SQLiteConnection.php";

Class Rating{
		
	public static function reportLeagueMatch($playerOneID, $playOneWins, $playerTwoID, $playTwoWins, $leagueID){
		$connection = new SQLiteConnection();
		
		$playerOneRating = $connection->getLeagueRating($playerOneID, $leagueID);
		$playerTwoRating = $connection->getLeagueRating($playerTwoID, $leagueID);
				
		$expectedScores = Rating::getExpectedScores($playerOneRating, $playerTwoRating);
		$playerOneExpectedScore = $expectedScores[0];
		$playerTwoExpectedScore = $expectedScores[1];
		
		if($playOneWins == 2){
			$playerOneScore = 1;
		}else if($playTwoWins == 2){
			$playerTwoScore = 1;
		}
			
		$newRatings = Rating::getNewRatings($playerOneScore, $playerOneRating, $playerOneExpectedScore, $playerTwoScore, $playerTwoRating, $playerTwoExpectedScore);
		$playerOneNewRating = $newRatings[0];
		$playerTwoNewRating = $newRatings[1];
		
		$connection->setLeagueRating($playerOneID, $playerOneNewRating, $leagueID);
		$connection->setLeagueRating($playerTwoID, $playerTwoNewRating, $leagueID);
		
		$connection->submitLeagueMatch($playerOneID, $playOneWins, $playerTwoID, $playTwoWins, $leagueID);
		
		$response['playerOneRating']   = $playerOneRating;
		$response['playerTwoRating']    = $playerTwoRating;
		$response['playerOneNewRating'] = $playerOneNewRating;
		$response['playerTwoNewRating'] = $playerTwoNewRating;
		
		return $response;
		
	}
	
	private static function getExpectedScores($playerOneRating, $playerTwoRating){
		$scores[0] = 1 / ( 1 + ( pow( 10 , ( $playerTwoRating - $playerOneRating ) / 400 ) ) );
        $scores[1] = 1 / ( 1 + ( pow( 10 , ( $playerOneRating - $playerTwoRating ) / 400 ) ) );
		return $scores;
	}
	
	private static function getNewRatings($playerOneScore, $playerOneRating, $playerOneExpectedScore, $playerTwoScore, $playerTwoRating, $playerTwoExpectedScore)
    {
        $newRatings[0] = round($playerOneRating + ( Config::KVALUE * ( $playerOneScore - $playerOneExpectedScore ) ));
		$newRatings[1] = round($playerTwoRating + ( Config::KVALUE * ( $playerTwoScore - $playerTwoExpectedScore ) ));
		return $newRatings;
	}
	
}
