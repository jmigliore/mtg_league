<?
include_once $_SERVER['DOCUMENT_ROOT'] . "/mtg_league/app/SQLiteConnection.php";

Class Validator{
	
	
	public static function validateSignUp($playerID,$leagueID){
		$connection = new SQLiteConnection();
		
		$rating = $connection->getLeagueRating($playerID, $leagueID);
		
		if($rating){
			return "That player is already signed up for that league";
		}else{
			return 1;
		}
		
	}
	
	public static function validateLeagueMatch($playerOneID, $playerOneWins, $playerTwoID, $playerTwoWins, $leagueID){
		
		if($playerOneID == $playerTwoID){
			return "There was an issue recording your match.";
		}else if($playerOneWins != 2 && $playerTwoWins != 2){
			return "There was an issue recording your match.";
		}else if($playerOneWins == 2 && $playerTwoWins == 2){
			return "There was an issue recording your match.";
		}else{
			return 1;
		}
	}
}
