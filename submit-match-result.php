<?php include_once "./app/SQLiteConnection.php"; ?>
<?php include_once "./app/Rating.php"; ?>

<?php include_once "./header.php" ?>

<div class="container my-2">
	<h2>Submit Match Result</h2>
	<form role="form" id="match-form">
		<div class="form-row">
			<div class="col-6">
				<label class="p-2">Player 1</label>
			</div>
			<div class="col-6">
				<label class="p-2">Wins</label>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<select class="form-control" id="playerOneID" required>
					<option></option>
					<?php
						$connection = new SQLiteConnection();
					
						$results = $connection->getLeaguePlayers(Config::ACTIVE_LEAGUE);
						foreach ($results as $player) {
							echo "<option value=" . $player['idplayer'] .">" . $player['name'] . "</option>";
						}
					?>
				</select>
			</div>
			<div class="col-6">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">				
					<label class="btn btn-light">
						<input type="radio" name="playerOneWins" required id="playerOneWins2"> 2
					</label>
					<label class="btn btn-light">
						<input type="radio" name="playerOneWins" required id="playerOneWins1"> 1
					</label>
					<label class="btn btn-light">
						<input type="radio" name="playerOneWins" required id="playerOneWins0"> 0
					</label>
				</div>
			</div>	
		</div>	
		<div class="row">
			<div class="col-6">
				<label class="p-2">Player 2</label>
			</div>
			<div class="col-6">
				<label class="p-2">Wins</label>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<select class="form-control" id="playerTwoID" required>
					<option></option>
					<?php
						foreach ($results as $player) {
							echo "<option value=" . $player['idplayer'] .">" . $player['name'] . "</option>";
						}
					?>
				</select>
			</div>
			<div class="col-6">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">				
					<label class="btn btn-light">
						<input type="radio" name="playerTwoWins" required id="playerTwoWins2"> 2
					</label>
					<label class="btn btn-light">
						<input type="radio" name="playerTwoWins" required id="playerTwoWins1"> 1
					</label>
					<label class="btn btn-light">
						<input type="radio" name="playerTwoWins" required id="playerTwoWins0"> 0
					</label>
				</div>
			</div>	
		</div>			
		<div id="feedback" role="alert"></div>
		<button type="submit" class="btn btn-primary mt-2" id="submit">Submit</button>
	</form>
</div>
	
<script type="text/javascript">
    $(document).ready(function() {
        //Ajax
        var submitted = false;
        $('#match-form').on('submit', function(event) {
        	
            var data = {};
            var playerOneWins;
            var playerTwoWins;
            
            if($('#playerOneWins2').is(':checked')){
            		playerOneWins = 2;
            }else if($('#playerOneWins1').is(':checked')){
            		playerOneWins = 1;
            }else if($('#playerOneWins0').is(':checked')){
            		playerOneWins = 0;
            }
            
            if($('#playerTwoWins2').is(':checked')){
            		playerTwoWins = 2;
            }else if($('#playerTwoWins1').is(':checked')){
            		playerTwoWins = 1;
            }else if($('#playerOneWins0').is(':checked')){
            		playerTwoWins = 0;
            }
            
            //Validation
            if(playerOneWins == 2 && playerTwoWins == 2){
            		$('#feedback').html("Only one player can have 2 wins.");
            		$('#feedback').addClass("mt-2 alert alert-danger");
            		return false;
            }else if(playerOneWins != 2 && playerTwoWins != 2){
            		$('#feedback').html("At least one player must have 2 wins.");
            		$('#feedback').addClass("mt-2 alert alert-danger");
            		return false;            	
            }
         
            if($('select[id=playerOneID]').val() == $('select[id=playerTwoID]').val()){
            		$('#feedback').html("You cannot play yourself.");
            		$('#feedback').addClass("mt-2 alert alert-danger");
            		return false;               	
            }
            
            data.service = 'SUBMIT_MATCH_RESULT';
            data.playerOneID   =     $('select[id=playerOneID]').val();
            data.playerTwoID   =     $('select[id=playerTwoID]').val();  
            data.playerOneWins =     playerOneWins;
            data.playerTwoWins =     playerTwoWins;
            data.leagueID      =  	<?php echo Config::ACTIVE_LEAGUE; ?>;

            if(!submitted){
                submitted = true;

                $.ajax({
                    type:		'POST',
                    data:		data,
                    url:		'./ajax/ajax-handler.php',
                    dataType:	'json',
                    error:		function(jqXHR,textStatus,errorThrown){},
                    success:	function(response,textStatus,jqXHR){
                        if (response.error) {
                            alert(response.error);
                            submitted = false;
                        } else {
							var html = "<div class='col-sm-12 text-center'><h1>Your match has been recorded.</h1><p>";
							var playerOneDelta = parseInt(response.playerOneNewRating, 10) - parseInt(response.playerOneRating, 10);
							var playerTwoDelta = parseInt(response.playerTwoNewRating, 10) - parseInt(response.playerTwoRating, 10);
							html += $('#playerOneID option:selected').text() + "'s new rating is " + response.playerOneNewRating + "(" + playerOneDelta + ").</p>";
							html += $('#playerTwoID option:selected').text() + "'s new rating is " + response.playerTwoNewRating + "(" + playerTwoDelta + ").</p>";
							html +="</div>";
							$('#match-form').html(html);
                        }
                    },
                    complete:	function(jqXHR,textStatus){}
                });
            }
            return false;

        });
    });
</script>
<?php include_once "./footer.php" ?>